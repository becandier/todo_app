import 'package:flutter/material.dart';

const Color myRed = Color.fromARGB(255, 199, 49, 30);

const Color myBlue = Color.fromARGB(255, 8, 56, 161);

const Color myBlack = Color.fromARGB(255, 34, 15, 5);

const Color myGray = Color.fromARGB(255, 66, 66, 66);

const Color myWhite = Color.fromARGB(255, 236, 236, 236);
