import 'package:flutter/material.dart';

class ToDo {
  String? id;
  String? todoText;
  bool isDone;

  ToDo({
    required this.id,
    required this.todoText,
    this.isDone = false,
  });

  static List<ToDo> toDoList() {
    return [
      ToDo(id: '01', todoText: 'Начать работу'),
      ToDo(id: '02', todoText: 'Скачать приложение', isDone: true),
    ];
  }
}
